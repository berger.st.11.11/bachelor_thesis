using Graphs, SimpleWeightedGraphs
using LinearAlgebra
using Plots
using ManifoldInterpolation

system_path = "./code/"

"""
    This file shows how to load a mesh into the Custom Graph datatype, compute the Laplace matrix and its eigenvalues and eigenvectors. 
"""

f(p,arg::Number) = diagm([1,arg,0]) * p  # scale y-direction
norm_eigenfunctions = zeros(3)
graphs = [nothing]
p = plot()
j = 2
param_space =  0.2:0.1:3


for i in param_space
    graph_tmp = CustomGraph(system_path * "Meshes/square.vtk")
    graphs = vcat(graphs,graph_tmp)
    loadMesh(graphs[j])
    transformMesh(graphs[j],f,i)
    createLaplacian(graphs[j])
    computeEigenGraph(graphs[j])
    norm_eigenfunctions = hcat(norm_eigenfunctions, distanceEigenfunctions(graphs[2],graphs[j]))

    j = j + 1
end 
norm_eigenfunctions = norm_eigenfunctions[:,2:end]
graphs = graphs[2:end]

##  adjencency Matrix
heatmap(Matrix(graphs[1].g.weights))

## eigenvalues
plot(graphs[1].Σ[1:100])

## saving results
saveResults(graphs[1], system_path * "Results/" * "one")
saveResults(graphs[end], system_path * "Results/" * "last")



## Trafo
# f(p) = p  # no trafo
f(p,arg::Number) = diagm([1,arg,0]) * p  # scale y-direction
# f(p) = map(p-> exp(p), p)
# f(p) = map(p-> p +  0.3* rand(3), p)


graph_untransformed = CustomGraph(system_path * "Meshes/plane_uniform.vtk")
loadMesh(graph_untransformed)
createLaplacian(graph_untransformed)
computeEigenGraph(graph_untransformed)

graph_transformed = CustomGraph(system_path * "Meshes/plane_uniform.vtk")
loadMesh(graph_transformed)
transformMesh(graph_transformed,f,2)
createLaplacian(graph_transformed)
computeEigenGraph(graph_transformed)


# eigenvectors
plot([graph_transformed.U[:,i]' * graph_untransformed.U[:,i] for i in 1:50])


# eigenvalues
p = plot()
plot!(p,graph_transformed.Σ[:],label="transformed")
plot!(p,graph_untransformed.Σ[:],label="un-transformed")


# eigenfunctions 
p = plot()
plot!(p,graph_transformed.U[:,1],label="transformed")
plot!(p,graph_untransformed.U[:,1],label="un-transformed")

# saveResults(graph_transformed, system_path * "Results/" * "transformed")
# saveResults(graph_untransformed, system_path * "Results/" * "un-transformed")