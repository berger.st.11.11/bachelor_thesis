# TODO Bachelor thesis

## Topics where work need to be done:

### Comparison Interpolation Methods
- [ ] splines vs polynomial 
- [ ] locally adaptive splines 

### Safety Margins
- [X] Understanding 
- [X] when there is an eigenvalue crossing, why are the interpolated eigenvectors bad
- [X] how to compute eigenvalues to find out which eigenvectors to use
- [X] what heuristics to use in application ? 
- [ ] Create nice Plots
- [ ] Summarize Result  
- [ ] Rayleigh Ritz Approach
  - [ ] subspace of Gr not necessary in Gr_safety → error in Gr_safety can be worse


### Complex Application
- [ ] What examples to use?
    - First which use case of the graph Laplacian ?
        - Spectral decomposition
        - ?

- [X] What if the Laplacian matrix does not have full rank?
    - Biggest component of the graph
    - Optimization Problem
- [ ] Spectral decomposition
    - Social Network which is evolving
      - https://networkrepository.com/dynamic.php
    - 3D Model 
      - [X] Choose which 3D Model to focus on
          - 3D Model made in Blender and animated
          - 3D Model generated from Lidar 
          - ?



## Topics for the written Part of the Thesis

- [ ] Boundary Conditions Graph Laplacian

### Combining Density Operator and Graph Manifold Interpolation

### Convergence Plots

### Check Comparison Interpolation Methods

### Think about the general Story I want to tell
