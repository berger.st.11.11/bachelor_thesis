#!/bin/bash

for file in thesis presentation abstract
do
latexmk $file.tex -synctex=1 -interaction=nonstopmode -file-line-error -pdf
done
