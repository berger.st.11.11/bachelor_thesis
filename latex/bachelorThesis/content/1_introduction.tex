% **************************************************************************** %
\chapter{Introduction}\label{ch:introduction}
% **************************************************************************** %

Quantum chemistry is a field that aims to create models that describe the properties of molecules and materials by describing their behavior on an atomic scale. These models aim to capture the behavior and interactions of the building blocks of matter, electrons, and nuclei. With those models, we then hope to learn more about macroscopic properties, because many macroscopic properties come from interactions that happen at the atomic scale. For example, the color of compounds or the difference between conductors and insulators.

The description of quantum systems, however, differs from the macroscopic world we are all familiar with. Rutherford proposed one of the first scientific models of atomic systems around 1910 \cite{rutherford1911lxxix}. It is the cartoonish picture that comes to most people's minds when they think about atoms. In the Rutherford model, there is a positively charged nucleus, that holds almost all the mass of the atom. And far away from the nucleus, the negatively charged electrons orbit around the center. This model had some serious flaws. Nothing is holding the electrons in their respective orbits. If this model would be true, every atom in the universe would almost instantly collapse, because of the electrostatic force between negatively charged electrons and positively charged nuclei. Niels Bohr proposed an improved model in 1913\cite{bohr}. In the Bohr model, electrons are only allowed on certain orbitals. This proposal would also explain an observed physical phenomenon. Scientists noticed that when you excite atoms, you could only measure photons with certain energies. Bohr proposed that those measured energies are the differences between two orbitals. But at the time, Bohr had no explanation, for why the elections would stay in their orbit. It would take another 13 years and the introduction of quantum mechanics through Erwin Schrödinger until the exact orbitals of the hydrogen atom were computed.

Erwin Schrödinger proposed the Schrödinger equation \eqref{eq:SchrodingerEquation1} and his version of quantum mechanics, called wave mechanics, in 1926 \cite{schrodinger1926quantisierung}. The Schrödinger equation \eqref{eq:SchrodingerEquation1} is a linear partial differential equation that describes the evolution of a quantum system:

\begin{align}
    i \hbar \frac{\partial }{\partial t}\psi(t) = H \psi(t) .
    \label{eq:SchrodingerEquation1}
\end{align}

Where $\psi$ is the wave function, $i$ is the imaginary number, $\hbar$ is a constant, and $H$ is the Hamilton operator. The wave function $\psi$ fully describes the state of the quantum system. Looking at the Schrödinger equation, we can already make a guess that $\psi$ is a function from a complex-valued function space. The Hamilton operator $H$ describes the total energy of the system. In most models, the Hamilton consists of two contributions: the kinetic energy of the particles and the potential energy that comes from the electrostatic Coulomb interactions:

\begin{align}
    H = T + V .
\end{align}

The ideas from the beginning of the 19th century up to around 1926 were combined and led to the formulation of the 6 postulates of quantum mechanics. Those postulates can not be derived, however, none of the postulates was yet disproven, and they continue to produce predictions that then can be verified by experiments.
Quantum chemistry is mainly interested in finding the ground state or lowest energy level of a quantum system. This is because, in nature, most molecules can be found in their lowest energy configuration. From the second postulate of quantum mechanics, we know that the possible measurable states of the system are the eigenvalues of a self-adjoint operator. Therefore, we need to solve for the lowest eigenvalue of the energy operator $H$ to find the lowest possible energy state:

\begin{align}
    H \psi = E \psi .
    \label{eq:stationarySchrodinger}
\end{align}

The equation \eqref{eq:stationarySchrodinger} is also called the time-independent Schrödinger equation. While the equation \eqref{eq:stationarySchrodinger} can be solved analytically for a hydrogen atom with 1 proton and 1 electron, this is not possible for larger systems with more electrons. This comes from the fact that the solution space grows exponentially with the number of electrons and protons.


However, the solution of equation \eqref{eq:stationarySchrodinger} is very hard to compute. This comes from the fact, that $\psi$ is very high dimensional. To get results for problems, approximations need to be made. A widely used approximation is the Born-Oppenheimer approximation. Since electrons and nuclei have very different masses but experience the same magnitude of force, electrons move much faster than the nuclei. Therefore, you can assume that the nuclei are stationary when solving for the electron state. This approximation works well for molecules in their respective ground state but gets worse for exited systems. With this approximation, we reduced the solution space from $\mathbb{R}^{3(n+m)}$ to $\mathbb{R}^{3n}$ where $n$ is the number of elections and $m$ is the number of nuclei.

However, this is still not enough to compute results for real systems with more than a couple electrons. Therefore, more approximations and different models have to be introduced. One of the first introduced models for quantum chemistry is the Hartree--Fock model. Hartree--Fock reduces the solution space by only looking at wave functions that are anti-symmetrical and can be constructed by a Slater determinant.

Recently there has been a trend to describe quantum systems not by a wave functions, but by projectors. Those projectors are called density operators and project to the space spanned by the wave functions. This has a few advantages which will be explained in this thesis.

% **************************************************************************** %
\section{Thesis Objectives and Outline}\label{sec:objectives}
% **************************************************************************** %
This thesis aims to give an introduction to the density operator formalism. In chapter \ref{ch:MathematicalFoundations} an introduction to the postulates of quantum mechanics and functional analysis is given. The goal is to get the reader without a mathematical or physics background to a level where they can understand the content of the next chapter. Chapter \ref{ch:densityOperator} then introduces the density operator in the context of eigenvalue problems. Special focus is set on the derivation of its properties and a comparison to the commonly used state space formulation using wave functions. Subsequently, an example of the application of the density operator in the context of Hartree--Fock is given.

The fourth and fifth chapters use the density matrices and bring them into context with differentiable geometries. In the fourth chapter, an introduction to Riemannian manifolds is given. Special focus is set on the Grassmann and Stiefel manifold. After that, in the fifth chapter, different interpolation algorithms for eigenfunctions on manifolds are explored. As an example, the interpolation algorithms will be applied to the graph Laplace operator, but the used techniques can be applied to any parameter-dependent operator.