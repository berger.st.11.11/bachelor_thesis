% **************************************************************************** %
\chapter{Mathematical Foundations}\label{ch:MathematicalFoundations}
% **************************************************************************** %
This chapter is an introduction to the mathematical concepts for understanding the theory behind the density operator. The goal of this thesis is to give a comprehensive overview of the density operator formalism. This cannot be done without some knowledge of Hilbert spaces and operators. Therefore, this chapter should give the reader a concise overview of the mathematical foundations upon which the density operator is built. But before starting with the introduction to Hilbert spaces, the postulates of quantum mechanics will be presented. This is because it motivates the usage of Hilbert spaces and shows the importance of self-adjoint operators. Also, in the next chapter, we will use them in order to derive an expression for the ground state energy. The sources for this chapter are \cite{reed2012methods}, \cite{gustafson2003mathematical}, \cite{dirac1981principles} and \cite{von2013mathematische}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Postulates of Quantum Mechanics} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
As a motivation for the introduction of the Hilbert spaces and operators, we first present the postulates of quantum mechanics.
Quantum mechanics as we know it, cannot be derived from anything. It needs some sort of foundation upon which the rest theory is built. The postulates of quantum mechanics as we know them today were introduced by von Neumann \cite{von2013mathematische} and Dirac \cite{dirac1981principles} around 1930. However, they are spread over many pages and were not condensed to a list. Therefore, we will be focusing on the postulates summarized by David McIntyre \cite{mcintyre2012quantum}.  

\begin{enumerate} 
	\item The state of a quantum system, including all the information you can know about it, is represented mathematically by a normalized element in a Hilbert space.
	\item A physical observable is represented mathematically by a self-adjoint operator acting on the Hilbert space.
	\item The only possible results of a measurement of an observable is one of the eigenvalues $a_n$ of the corresponding operator $A$.
	\item The probability of obtaining the eigenvalue $a_n$ in a measurement of the observable $A$ on the system in the state $\psi$ is: 
	\\ \begin{align*}
		 P_{a_n} = |(a_n, \psi)|^2 .
	\end{align*}
		($a_n$ is the eigenvector corresponding to the eigenvalue $a_n$ in this context)
	\item After a measurement of $A$, that yields the result $a_n$, the quantum system is in a new state that is the normalized projection of the original system state onto the state corresponding to the result of the measurement.
	\item The time evolution of a quantum system is determined by the Hamiltonian through the Schrödinger equation: \\
		\begin{align*}
			i \hbar \frac{\partial }{\partial t}\psi(t) = H \psi(t) .
			\label{eq:SchrodingerEquation}
		\end{align*}
\end{enumerate}

As you can see, you can not describe a quantum state without a Hilbert space. The second postulate to the fifth, all depend on knowledge about operators and their corresponding spectrum. Therefore, in the next section we will define Hilbert spaces and then in the one after that, operators acting on those Hilbert spaces will be introduced.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Hilbert Spaces}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We start with the definition of the vector space. The most know vector space is the Euclidean space $\mathbb{R}^n$, which is extensively used in classical physics.

\begin{definition}[Vector Space]~\\
	A vector space $\mathcal{V}$ is a collection of elements (denoted here as: $u,v,w,\dots$), called vectors, for which the operations of addition and multiplication by a complex number are defined such that:
		\begin{align*}
			(\text{i})& \; u + v = v + u \quad  && \text{(commutativity)} \\
			(\text{ii})& \; u + (v + w) = (u + v) + w \quad && \text{(associativity)} \\
			(\text{iii})& \; u + 0 = 0 + u = u \quad \quad && \text{(existence of a 0 vector)} \\
			(\text{iv})& \; \alpha(\beta u) = (\alpha \beta)u   \\
			(\text{v})& \; \alpha u + \beta u = (\alpha + \beta) u   \\
			(\text{vi})& \; \alpha (u + v) = \alpha u + \alpha v   \\
			(\text{vii})& \; 0 u = 0,\; 1u = u   \\
		\end{align*}
\end{definition}

Now we add more structure to vector spaces. By defining a norm, we can compare vectors to each other.

\begin{definition}[Normed Vector Space]~\\
	A normed linear space $\mathcal{V}$ is a vector space over $\mathbb{C}$ and a function $\| \cdot \|$ from $\mathcal{V}$ to $\mathbb{R}$  which satisfies:
	\begin{align*}
		(\text{i})& \; \| v \| \ge 0 \quad &&\forall v \in \mathcal{V} \\
		(\text{ii})& \; \| v \| = 0 \quad &&\text{if and only if } v = 0 \\
		(\text{iii})& \; \| \alpha v \| = |\alpha| \| v \| \quad &&\forall v \in \mathcal{V} \text{ and } \alpha \in \mathbb{C} \\
		(\text{iv})& \; \| v + w \| \le  \| v \| + \| w \| \quad &&\forall v,w \in \mathcal{V}
	\end{align*}
\end{definition}

Normed vector spaces are useful, but in general, they are not complete. For many applications this is unsatisfactory. Therefore, we need to define complete normed spaces. Complete normed vector spaces are called Banach spaces. To define Banach spaces, we need Cauchy sequences.

\begin{definition}[Cauchy Sequence]~\\
	A sequence of vectors $\{ v_n \}$ of a normed vectors space $\mathcal{V}$ is called a Cauchy sequence if:
	\begin{align}
		(\forall \epsilon) (\exists N)\ : \ n,m \ge N \text{ implies } \| x_n - x_m \| < \epsilon .
	\end{align}
\end{definition}

\begin{definition}[Completeness]~\\
	A normed vector space $\mathcal{V}$ in which all Cauchy sequences converge to an element in $\mathcal{V}$ is called complete.
\end{definition}

\begin{example}[Completeness]~\\
	A classical example of incompleteness is the space of rational numbers $\mathbb{Q}$ with Euclidean norm. This is because the Cauchy sequence $x_1=1$ and $x_{n+1} = \frac{x_n}{2} + \frac{1}{x_n}$ has the limit $\sqrt{2}$, however $\sqrt{2}$ is not in $\mathbb{Q}$. \\
	Examples of complete spaces would be $\mathbb{R}$ and $\mathbb{C}$ or  $\mathbb{R}^n$.
\end{example}

\begin{definition}[Banach Space]~\\
	A complete normed vector space is called Banach space.
\end{definition}

We now have defined Banach spaces, but we can introduce spaces with even more structure. This structure gives vector spaces a sense of direction. Those spaces are called Hilbert spaces and are essential in quantum physics as we have seen from the first postulate of quantum mechanics.

\begin{definition}[Inner Product Space]~\\
	A complex vector space $\mathcal{V}$ is called an inner product space if there is a complex-valued function $(\cdot,\cdot)$ on $\mathcal{V} \times \mathcal{V}$ that satisfies the following four conditions for all $x,y,z \in \mathcal{V}$ and $\alpha \in \mathbb{C}$:
	\begin{enumerate}[label=(\roman*)]
		\item $(x,x) \geq 0 \quad \text{and} \quad (x,x) = 0 \quad \text{if and only if} \quad x = 0$
		\item $(x ,y + z) = (x,y) + (x,z)$
		\item $(x,\alpha y) = \alpha (x,y)$
		\item $(x,y) = \overline{(y,x)} $
	\end{enumerate}
	The function $(\cdot, \cdot)$ is called inner product.
\end{definition}

\begin{theorem}[Norm]~\\
	Every inner product space $\mathcal{V}$ is a normed linear space with the norm $\|x\|= \sqrt{(x,x)}$.
\end{theorem}

\begin{definition}[Hilbert Space]~\\
	A complete inner product space is called a Hilbert space $\mathcal{H}$.
\end{definition}

\begin{example}[$L^2$ Space]~\\
	A very important Hilbert space, and the one we will be using during this thesis, is the space of square-integrable functions $L^2$:
	\begin{align}
		L^2(\mathbb{R}^d) = \{ v: \mathbb{R}^d \rightarrow \mathbb{C} \;|\; \int_{\mathbb{R}^d} |v|^2 dx  < \infty\}
	\end{align}
	with the inner product:
	\begin{align}
		(v,w)_{L^2} = \int_{\mathbb{R}^d} v^*w dx = v^*u ,
	\end{align}
	where $v^*$ denotes the conjugate complex of $v$. If thinking of vectors in terms of values in an array, then an element of $L^2$ is a column vector and the complex conjugate of it is a row vector.
\end{example}


For the last item in this section, we will now define the concept of orthonormality.

\begin{definition}[Orthonormality]~\\
	Two vectors $x,y$ in a Hilbert space $\mathcal{H}$ are said to be orthogonal if $(x,y) = 0$. A collection $\{x_{i}\}$ of vectors is called orthonormal set if $(x_i,x_i) = 1 \ \forall i$, and $(x_i,x_j) = 0$ if $i \neq j$.
\end{definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Operators}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

As seen in the postulates, operators play a huge role in quantum mechanics. Of special importance are self-adjoint operators, since their eigenvalues are the measurable values, also called observables. We are interested in finding the ground state energy of a quantum system. From the second postulate, we know that possible energy states are given by the eigenvalues of the energy operator. The source for this chapter is \cite{reed2012methods}.

\begin{definition}[Linear Operator]~\\
    A linear operator from a Hilbert space $\mathcal{H}_1$ to a Hilbert space $\mathcal{H}_2$ is a function, T, from $\mathcal{H}_1$ to $\mathcal{H}_2$ which satisfies:
	\begin{align}
		T(\alpha v + \beta w) = \alpha T(v) + \beta T(w) \quad (\forall v,w \in \mathcal{H}_1) (\forall \alpha,\beta \in \mathbb{C}) .
	\end{align}
\end{definition}

In order to define self-adjoint operators, we first need to define symmetric operators. Symmetric operators are operators, whose adjoint is equal to themselves. In the finite-dimensional case, symmetric and self-adjoint operators are the same. In the infinite-dimensional case, however, it gets a bit more technical.

\begin{definition}[Adjoint]~\\
	The adjoint of an operator $A$ acting on a Hilbert space $\mathcal{H}$, is the operator $A^*$ satisfying:
	\begin{align}
		(A^*v,w) = (v,A w)  \quad \forall v,w \in \mathcal{H}.
	\end{align}
\end{definition}

\begin{definition}[Symmetric Operator]~\\
	A linear operator $A$ acting on a Hilbert space $\mathcal{H}$, is called symmetric if:
		\begin{align}
		(Av,w) = (v,A w)\quad \forall v,w \in \mathcal{H}.
	\end{align}
\end{definition}

\begin{definition}[Self-adjoint]~\\
	A linear operator $A$ acting on a Hilbert space $\mathcal{H}$ is called self-adjoint if it is symmetric and $\text{Ran}(A \pm i)=\mathcal{H}$.
\end{definition}

A class of operators that is of special importance, are unitary operators. They prove especially useful in proving many theorems, because of the normality property that allows them to be inserted in many equations.

\begin{definition}[Unitary Operator]~\\
    A unitary operator is a bijective operator $U$ on a Hilbert space $\mathcal{H}$ such that:
    \begin{align}
        (Uv,Uw) =  (v,w) \quad \forall v,w \in \mathcal{H}.
    \end{align}
\end{definition}

\begin{lemma}
    The inverse operator $U^{-1}$ of a unitary operator $U$ is the adjoint of the operator $U$:
    \begin{align}
        U^{-1} = U^* .
    \end{align}
\end{lemma}

\begin{lemma}[Normality]~\\
    If $U$ is a unitary operator, then the following is true:
    \begin{align}
        U^* U = U U^* = I .
    \end{align}
\end{lemma}

Another class of operators that will be used later in this thesis are projectors:

\begin{definition}[Projector]~\\
	If $P$ is a bounded linear operator acting on the Hilbert space $\mathcal{H}$ and $P^2=P$, then P is called a projection. If in addition $P = P^*$, then $P$ is called an orthogonal projection.
\end{definition}

There is a good intuitive explanation for $P^2=P$. Applying multiple projections to an element should yield the same result as applying it once.

The spectrum of an operator is the generalization of eigenvalues from finite-dimensional matrices. As already seen in the postulates of quantum mechanics, the eigenvalues and eigenvectors are hugely important for the computation of the ground state energy and the corresponding system state.

\begin{definition}[Spectrum]~\\
	Let $A$ be an operator on the Hilbert space $\mathcal{H}$. Any $x \neq 0$ which satisfies $Ax = \lambda x$ for some $\lambda \in \mathbb{C}$ is called an eigenvector of $A$. $\lambda$ is called the corresponding eigenvalue. The set of all eigenvalues is called the spectrum of $A$.
\end{definition}