\chapter{Energy Based Interpolation}\label{ch:EnergyBasedInterpolation}
So far the interpolation of the smallest eigenvectors was done in the tangent space at a point on the manifold. In order to do so, the exponential and logarithmic maps need to be used to transform vectors from the manifold and to the tangent space and vice versa. The logarithmic, as well as the exponential map, require a singular value decomposition which is not a cheap operation. So instead of interpolating on the tangent space, we try to find a way to compute a parametrized curve $\gamma(t) \in St(n,p) \ \forall t \in [t_{\text{start}},t_{\text{end}}]$ which will be our new interpolation function. 

The idea is to state this as a variational problem, in which the start $\gamma(t_{start})$ and endpoint points $\gamma(t_{end})$ are given as our interpolation nodes. This value of the functional can be regarded as the energy that we are trying to reduce. Now, how would the functional of our problem look like? The first choice would be to use the Rayleigh quotient $R(\gamma(t)) = \frac{\gamma(t)^T A(t) \gamma(t)}{\gamma(t)^T \gamma(t)}$. Remember that for the eigenvectors corresponding to the smallest eigenvalues, the Rayleigh quotient is at a minimum. This means that in each point $\gamma(t)$ we want to have a minimum of the Rayleigh quotient.

\begin{problem}[Energy Based Interpolation]~\\ \label{pr:EnergyBasedInterpolation}
    Find the curve $\gamma_{opt}(t) \in St(n,p)$, such that the following integral is minimized:
    \begin{align*}
    \gamma_{opt}(t) &= \argmin \int_{t_{start}}^{t_{end}} \frac{\gamma(t)^T A(t) \gamma(t)}{ \gamma(t)^T \gamma(t)} dt \\
        \text{subject to}:&  \\
        & \gamma(t)^T\gamma(t) = \text{I} \quad \forall t \in [t_{\text{start}},t_{\text{end}}], \\
        & \gamma(t_{start}) = \gamma_{start}, \\
        & \gamma(t_{end}) = \gamma_{end}.
    \end{align*}
\end{problem}

Those types of functional can be solved using the Euler equation \cite{gelfand2000calculus}, which is also called the Euler-Lagrange equation:

\begin{theorem}[Euler Equation]~\\ \label{th:EulerEquation}
    Let $J[y]$ be a functional of the form
    \begin{align}
        J[y] = \int_a^b F(x,y,y')dx ,
    \end{align}
    defined on the set of functions $y(x)$ which have continuous first derivatives in $[a,b]$ and satisfy the boundary conditions $y(a)=A$,\ $y(b)=B$. Then a necessary condition of $J[y]$ to have an extremum for a given function $y(x)$ is that $y(x)$ satisfy the Euler's equation:
    \begin{align}\label{eq:EulerEq}
        \frac{\partial F}{\partial y} - \frac{\text{d}}{\text{d}x} \frac{\partial F}{\partial y'} = 0.
    \end{align}
\end{theorem}
\par

Applying the Euler equation to our functional formulated in \ref{pr:EnergyBasedInterpolation} leads to the following partial derivatives:

\begin{align}
    F(t,\gamma,\gamma') &= \frac{\gamma(t)^T A(t) \gamma(t)}{ \gamma(t)^T \gamma(t)}, \\
    \frac{\partial F}{\partial \gamma'} &= 0, \\
    \frac{\partial F}{\partial \gamma} &= \frac{2}{\| \gamma \|^2} \left(A(t) -  \frac{\gamma(t)^T A(t) \gamma(t)}{ \gamma(t)^T \gamma(t)} \right) \gamma(t).
\end{align}

Plugin those partial derivatives into Euler equation \ref{eq:EulerEq} gives the following equation:

\begin{align}\label{eq:EulerApplied}
    \frac{2}{\| \gamma \|^2} \left(A(t) -  \frac{\gamma(t)^T A(t) \gamma(t)}{ \gamma(t)^T \gamma(t)} \right) \gamma(t) = 0.
\end{align}

The expression $\left(A(t) -  \frac{\gamma(t)^T A(t) \gamma(t)}{ \gamma(t)^T \gamma(t)} \right) \gamma(t)$ is also called the spectral residual \cite{LambertStamm}. The spectral residual only vanishes if $\gamma(t)$ is an eigenvector of $A(t)$.  From this, we see that the result of equation \ref{eq:EulerApplied} is only a necessary condition for extreme points of the functional, and not sufficient. This is because the spectral residual would vanish for any eigenvector, not only the ones we are trying to interpolate.  However, we can still gain some insight into how an interpolation function on the manifold would be computed. 

Equation \ref{eq:EulerApplied} has to be valid in the whole interpolation domain $ t \in [t_{\text{start}},t_{\text{end}}]$. This means that for every $t$, an eigenvalue problem has to be solved and no information from either $t+\epsilon$ or $t-\epsilon$ can be used to accelerate the computation. So it seems that by formulating the interpolation problem as a variational problem we do not gain a computational advantage.

However, we can still salvage the result. An alternative to the Euler equation is to discretize the functional and compute each optimal point of the trajectory individual. We discretize the functional with a simple quadrature rule:
\begin{align}
    \int_{t_{start}}^{t_{end}} \frac{\gamma(t)^T A(t) \gamma(t)}{ \gamma(t)^T \gamma(t)} dt \ \approx \ \sum_{i=1}^N \frac{\gamma_i^T A_i \gamma_i}{ \gamma_i^T \gamma_i} \Delta t .
\end{align}

Now we can optimize the discretized functional at every point $\gamma_i$, which leads to the following necessary optimality condition for each quadrature point:
\begin{align}
    \frac{\partial F}{\partial \gamma_i} =  \frac{2 \Delta t}{\| \gamma \|^2}  \left(A_i -  \frac{\gamma_i^T A_i \gamma_i}{ \gamma_i^T \gamma_i} \right) \gamma_i = 0 .
\end{align}
The equation above is similar to the expression \ref{eq:EulerApplied} found for the continuous case. However, the discrete formulation may have a use case for constructing a hierarchical interpolation algorithm using the Bellman optimality condition. Simply speaking, the Bellman optimality condition states that when an optimal trajectory is split into multiple trajectories, each of the new trajectories is optimal again. This means that we can find a globally optimal point for a parameter $t_{mid}$ and then divide the problem into two new, but smaller problems as demonstrated in figure \ref{fig:HierarchicalInterpolation}. We can find those globally optimal points by computing an eigenvalue decomposition or by finding a minimum of the spectral residual with an optimization algorithm like gradient descent.

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale=1]{figs/HierarchicalInterpolation/ErrorInterpolation.pdf}
    \caption{This figure shows the idea behind the hierarchical interpolation. Each iteration step refines the optimal path.}
    \label{fig:HierarchicalInterpolation}
    \end{center}
\end{figure}

The idea of hierarchical interpolation can be further explored in subsequent work. Problems with this method are however already obvious. First, how to interpolate between the computed points? Because a straight line between two points on the Stiefel manifold is no longer on the manifold. A possible solution to this would be to compute the geodesics, which are the lines of the shortest distance on the manifold between two points. It would also be interesting to compare the quality and computational effort of this method to an approach on the tangent manifold as shown in this thesis using either splines or polynomial interpolation.