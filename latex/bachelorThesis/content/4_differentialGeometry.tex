%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Differential Geometry}\label{sc:DifferentialGeomertry}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \section{General Concepts}
Differentiable manifolds are smooth topological spaces that can locally be described by a Euclidean space. This thesis uses matrix manifolds as a mathematically elegant way to describe the evolution of eigenvectors of an operator depending on a parameter. In this chapter, two types of manifolds are introduced. The Stiefel manifold $St(n,p)$, which is an embedded manifold in $\mathbb{R}^{n \times p}$ that consists of orthogonal matrices, and the Grassmann manifold $Gr(n,p)$ which has a different structure than the Stiefel since it is a quotient manifold. The elements of the Grassmann manifold are $p$-dimensional subspaces of $\mathbb{R}^n$. Even though the manifolds may seem similar, they act as constraints to two different classes of problems. The first class of problems are the ones that depend on orthogonal matrices, for example, the minimization of $F(X)$, where $X$ is an orthogonal matrix. The other class of problems depends on the spanned space, instead of the selected basis, this means $G(X) = G(XQ)$, where Q is an orthogonal matrix. 

While it is possible to interpret embedded manifolds with geometric knowledge of $\mathbb{R}^{n \times p}$, this is not so easy for quotient manifolds. Therefore, the embedded manifold will be introduced first, and then the introduced concepts will be translated to the quotient manifold.

The formal definition of manifold requires the use of maps and atlases between a manifold $\mathcal{M}$ and the Euclidean space $\mathbb{R}^n$. For the sake of simplicity and our use cases, this is not necessary. Only a short introduction on manifolds is given and necessary results for computations will be presented. For a more detailed introduction we refer the reader to \cite{absil2009optimization} or any other book on differentiable manifolds.

When describing manifolds, you can distinguish between intrinsic and extrinsic coordinates. Extrinsic coordinates describe the manifold $\mathcal{M}\subset \mathbb{R}^{n \times p}$ from the perspective of the ambient space $\mathbb{R}^{n \times p}$. Intrinsic coordinates utilize a local parameterization. When handling manifolds in a numerical context, extrinsic coordinates are preferred.

Essential to many algorithms is the tangent space $T\mathcal{M}$ and its elements, the tangent vectors. Intuitively, the tangent space at $p \in \mathcal{M}$ is a plane spanned by the vectors that are tangent to $p$. A visualization of this can be seen in figure \ref{fig:tangentSpaceWithExpMap}.

\begin{definition}[Tangent Spaces]~\\
    Let $\mathcal{M}$ be a submanifold of $\mathbb{R}^{n \times p}$. The tangent space of $\mathcal{M}$ at a point $p \in \mathcal{M}$, in symbols $T_p \mathcal{M}$, is the space of velocity vectors of differentiable curves $c:t \mapsto c(t)$ passing through $p$, i.e.,
    \begin{align}
        T_p \mathcal{M} = \{ \dot c(t_0) \ | \ c: J \mapsto \mathcal{M}, \ c(t_0) = p \} ,
    \end{align}
    where $J \subseteq \mathbb{R}$ is an arbitrary small open interval with $t_0 \in J$.  
\end{definition}

Let the manifold be defined by a function $F(x) = 0$, then the set of all tangent vectors is given by:

\begin{equation}
    T_x \mathcal{M} = \ker(DF(x)), \label{eq:tangentSpaceEquation}
\end{equation}

where $Df(x)[\dot{\gamma}(0)] = \frac{\text{d}(F(\gamma(t)))}{\text{d}t}|_{t=0}$ describes the derivative of a curve $\gamma(t)$ on $\mathcal{M}$ with $\gamma(0) = x$. In other words, tangent vectors are those, that are orthogonal to the constraint of the manifold.

\begin{example}[Tangent Space on a Unit Sphere]~\\
    Each point on a unit sphere $S^{d-1}$  embedded in $\mathbb{R}^d$ is described by 
    \begin{align}
        F(x) =  x^T x -1 \quad \text{and} \quad S^{d-1} \coloneqq  \{ x \in \mathbb{R}^d \;|\; F(x) = 0 \}.
    \end{align}
    From this constraint follows that the tangent space at the point $x \in S^{d-1}$ is given by: 
    \begin{align}
        T_x S^{d-1} = \ker(D(F(x))) = \{ z \in \mathbb{R}^d \;|\; x^T z + z^T x = 0 \} = \{ z \in \mathbb{R}^d \;|\; x^T z = 0 \} .
    \end{align}
\end{example}

\begin{definition}[Riemannian Manifold]~\\
    A manifold $\mathcal{M}$ is called Riemannian manifold if it is equipped with a smooth varying inner product called Riemann metric:
    \begin{align}
        g_x(\xi ,\zeta), \quad  \quad \xi, \zeta \in T_x\mathcal{M} .
    \end{align}
\end{definition}

Manifolds that are embedded in $\mathbb{R}^{n \times p}$ inherit a Riemann metric from the embedded space. Let $\xi, \zeta \in T_x\mathcal{M}$, then the standard inner product leads to the following metric: 
\begin{align}
    g_x(\xi ,\zeta) = \Tr(\xi^T \zeta) .
\end{align}
For more details about metrics see the chapter 3.6 in \cite{absil2009optimization}.

\begin{definition}[Normal Space]~\\
    Let the Manifold $\mathcal{M}$ be embedded in $\mathbb{R}^{n \times p}$, then the orthogonal complement of $T_x \mathcal{M}$ in $\mathbb{R}^{n \times p}$ is called normal space to $\mathcal{M}$ at $x$:
    \begin{align}
        (T_x \mathcal{M})^{\perp} \coloneqq \{\zeta \in \mathbb{R}^{n \times p} \ | \ g_x(\zeta, \xi) = 0 \quad \forall \xi \in T_x \mathcal{M} \} .
    \end{align}
\end{definition}

\begin{lemma}
    Every element $ \xi \in \mathcal{M} \subset \mathbb{R}^{n \times p}$ can be uniquely decomposed into a tangent and normal space part:
    \begin{align}
        \xi = P_x(\xi) + P_x^{\perp}(\xi),
    \end{align}
    where $P_x$ is a projection onto the tangent space $T_x \mathcal{M}$ and $P_x^{\perp}$ is a projection onto the normal space. 
\end{lemma}


\section{Stiefel Manifold}
The Stiefel manifold $St(n,p) \subset \mathbb{R}^{n \times p}$ can be thought of as the set of $p$-dimensional orthogonal basis vectors embedded in $\mathbb{R}^{n \times p}$. For $p=1$ this results in all the points on the $n$-dimensional unit sphere. The Stiefel manifold is given by:
\begin{align}
    St(n,p) \coloneqq \{ X \in \mathbb{R}^ {n \times p} | X^T X = I_p \},
\end{align}
where $I_p$ is the $p$-dimension unity matrix.

The tangent space of the Stiefel manifold can be constructed by the equation presented in \eqref{eq:tangentSpaceEquation}:

\begin{align}
    T_x St(n,p) \coloneqq \{ \eta \in \mathbb{R}^{n \times p} \;|\; X^T \eta -  \eta^T X = 0 \}.
\end{align}


\section{Grassmann Manifold}
The Grassmann manifold $Gr(n,p)$ is a quotient manifold. Each point on the manifold denotes a $p$-dimensional subspace of $\mathbb{R}^{n}$. Another way to conceptualize this is to think in terms of equivalent classes $[\cdot]$: 

\begin{align}
    [X] = [Y] \; \Leftrightarrow \; \text{span}(X) = \text{span}(Y).
\end{align}

Each point on the manifold is an equivalent class of $p$-dimensional bases. The Grassmann manifold will play an important part in the interpolation of the density operator. This is because the density operator is a projection onto a $p$-dimensional space, that does not depend on the used basis. We can easily see that all $p$-dimensional density operators and the $p$-dimensional Grassmann manifold are isomorphic.

In numerical algorithms, elements of the Grassmann manifold are represented as elements of the Stiefel manifold. The Grassmann manifold is defined as:
\begin{align}
    Gr(n,p) = \{[U] \ | \ U \in St(n,p)\}.
\end{align}

The tangent space of $Gr(n,p)$ at a representative $U \in St(n,p)$ is given by:
\begin{align}
    T_U Gr(n,p) = \{ \eta \in \mathbb{R}^{n \times p} \ | \ U^T \eta = 0 \}.
\end{align} 


\section{Geodesics and the Exponential and Logarithmic maps}

In this section, geodesics, as well as the exponential and logarithmic maps, will be introduced. Geodesics generalize the notation of the shortest distance between two points on manifolds. Building upon geodesics, exponential maps allow us to project vectors from the tangent space $\eta \in T_p\mathcal{M}$ to the manifold $q = \textnormal{Exp}_p(\eta) \in \mathcal{M}$. The logarithmic map does the inverse to the exponential map and gives us a tangent vector $\eta$ that leads from one point to another one $\eta = \textnormal{Log}_p(q) \in T_p \mathcal{M}$. The exponential and logarithmic maps are important to the interpolation algorithm presented in chapter \ref{ch:InterpolationOnManifolds}.

\begin{theorem}[Geodesics]~\\
    Let $\mathcal{M}$ be a Riemannian manifold with the metric $g_p(\zeta,\xi)$ where $\zeta,\ \xi \in T_p \mathcal{M}$. A geodesic, $\gamma : [a,b]\rightarrow \mathcal{M}$ is a smooth curve that minimizes the distance $L(\gamma)$ between two points on $\mathcal{M}$. The distance of a curve $\gamma$ is given by:
    \begin{align}
        L(\gamma) = \int_a^b \sqrt{g_{\gamma(t)}(\dot{\gamma}(t),\dot{\gamma}(t))}dt.
    \end{align}
\end{theorem}

Besides reducing the distance between two points, geodesics are curves with no acceleration, i.e. straight lines. Using geodesics, you can construct the exponential map and logarithmic map. The exponential map takes a direction and length $\eta \in T_p \mathcal{M}$ and return a point $q \in \mathcal{M}$ if you start at $p \in \mathcal{M}$ with the velocity $\eta$.

\begin{definition}[Grassmann exponential map]~\\
    Let $p_0 \in \mathcal{M} = Gr(n,p)$ and $\eta \in T_p \mathcal{M}$. Furthermore, let $ \eta = USV$ be the singular value decomposition of $\eta$, then the exponential map can be computed by:
    \begin{equation}
        \textnormal{Exp}_p^{Gr} (\eta) = p_0 V\cos(S)V^T \ + \ U\sin(S)V^T = q \in \mathcal{M},
    \end{equation}
    where $\cos$ and $\sin$ are applied to each component of $S$.
\end{definition}

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale=1.5]{figs/ManifoldAndTangentSpace/ManifoldAndTangentSpace.pdf}
    \caption{This figure shows the Manifold $\mathcal{M}$ in black and the tangent space spanned at $p$ in red. The exponential map is applied on $\eta \in T_p\mathcal{M}$, which returns the element $q \in \mathcal{M}$.}
    \label{fig:tangentSpaceWithExpMap}
    \end{center}
\end{figure}

The logarithmic map is the inverse operation of the exponential map. Both the exponential and logarithmic maps are only locally defined, this could have implications for some numerical algorithms because $\textnormal{Exp}_p\textnormal{Log}_q p = p$ will not always hold true. 

\begin{definition}[Grassmann logarithmic map]~\\
    Let $p,q \in \mathcal{M} = Gr(n,p)$. The geodesic curve between $p$ and $q$ is given by:
    \begin{align}
        \textnormal{Log}_p^{Gr} q = V \arctan(S) U^T,
    \end{align}
    where the $\arctan$ is applied component-wise to $S$. $U, S$, and $V$ are from the singular value decomposition of 
    \begin{equation}
        USV = (q^Tp)^{-1}(q^T-q^Tpp^T).
    \end{equation} 
\end{definition}

After applying any transformation on an element from the Grassmann manifold, you can perform a QR-decomposition and use the Q matrix as your new element in order to increase numerical stability and get a cleaner-looking representation.

For more in-depth overview about manifolds, check \cite{absil2009optimization}, \cite{zimmermann2019manifold} or \cite{edelman1998geometry}. An implementation of most manifold operations on the most common manifolds for the julia programming language can be found at \cite{manifold.jl}.