% **************************************************************************** %
\chapter{Density Operator}\label{ch:densityOperator}
% **************************************************************************** %

This chapter introduces the density operator. But first, the Hamilton operator and time-independent Schrödinger equation will be treated. The general form of the Hamiltonian $H$ and the time-independent Schrödinger equation were already shown in the introduction. Now we will cover them in more detail. As mentioned before, the main objective of quantum chemistry is the computation of the ground state energy. This can be done by solving for the lowest eigenvalues of the time-independent Schrödinger equation:

\begin{align}
    H \psi = E \psi.
    \label{eq:timeIndependentSchrodinger}
\end{align}

As common in quantum chemistry, we will be working with atomic units:

\begin{align}
    m_e = 1, \ q_e = 1, \ \hbar = 1, \ \frac{1}{4\pi\epsilon_0} = 1 .
\end{align}

In the following we will be treating a quantum system with $N \in \mathbb{N}$ electrons with charge of $\{1\}_{i=1}^N$ at positions $\{x_i\}_{i=1}^N$. For the sake of simplicity, the spin variables will be omitted. This is because the presented models can easily be expanded to incorporate spin, however it would only make the notation more cumbersome. Besides the electrons, the system also consists of $M \in \mathbb{N}$ nuclei with charge $\{-Z_i\}_{i=1}^M$ at positions $\{X_i\}_{i=1}^M$ and with mass $\{M_i\}_{i=1}^M$. 

The first step for the ground state calculation is the modeling of the Hamilton operator $H$. The Hamilton operator describes the total energy of the system. It can be decomposed into multiple parts. Generally, it consists of two components, the kinetic energy $T$ as well as the potential energy $V$. Where the latter can be further decomposed into multiple contributions. Interactions on the atomic scale are dominated by the electrostatic force. This means we have to take the Coulomb interaction of electron-electron, nuclei-nuclei, and electron-nuclei into account. For this case, the general Hamiltonian operator $H$ reads:

\begin{align}
    H &= T_e + T_n + V_{ee} + V_{nn} + V_{en} ,
\end{align}

where

\begin{align}
    T_e &= -\sum_{i=1}^N \frac{\Delta_i}{2} \\
    T_n &= -\sum_{i=1}^M \frac{\Delta_i}{2M_i} \\
    V_{ee} &= \sum_{\substack{i,j=1 \\ i \neq j}}^{N}  \frac{1}{ |x_i - x_j |} \\
    V_{nn} &= \sum_{\substack{i,j=1 \\ i \neq j}}^{M}  \frac{Z_i Z_j}{ |X_i - X_j |} \\
    V_{en} &= \sum_{\substack{i=1}}^{N} \sum_{\substack{j=1}}^{M} -\frac{Z_i}{ |x_i - X_j |} .
\end{align}

Also, as already mentioned in the introduction, such problems are hard to solve because of the high dimensionality of the wave function. In order to reduce the dimension, we will be working with the Born-Oppenheimer approximation. The approximation uses the fact that the nuclei are a couple of magnitudes heavier than the electron and since they experience the same magnitude of force, the electrons move much faster. This means that from the electron's point of view the nuclei are basically standing still. Using this, we can split the problem \eqref{eq:timeIndependentSchrodinger} into two parts, one for the elections and one for the nuclei. In this thesis, we will focus on the electrons. With this assumption, we can then formulate the electron Hamiltonian $H_e$ which just describes the state of the electrons and treats the electric field produced by the nuclei as an external potential that confines the electrons:

\begin{align}
    H_e = T_e + V_{en} + V_{ee} .
\end{align}

With this approximation we can simplify the time independent Schrödinger equation \eqref{eq:timeIndependentSchrodinger} to:

\begin{align}
    H_e \psi = E \psi .
    \label{eq:electronicHamiltion}
\end{align}

\begin{theorem}[Energy Functional]~\\
    The energy of a quantum state given by a wave function $\psi$ can now be computed as the expectation value of said state with the electronic Hamiltonian $H_e$:
    
    \begin{align}
        E(\psi) = \int_{ \mathbb{R}^{3N}} \psi^* H_e \psi \ dx_1, \dots, dx_N .
        \label{eq:energyFunctional}
    \end{align}
\end{theorem} 
\begin{proof} 
    Let $u_i$ be the $i$-th eigenvector corresponding to the eigenvalue $\lambda_i$ of the operator $E$.
    We know from the fourth postulate of quantum mechanics that the probability $P_{\lambda_i}$ of measuring $\lambda_i$ from the state $\psi$ is:
    \begin{align}
        P_{\lambda_i}(\psi) = |(u_i,\psi)|^2 .
    \end{align}
We start with the average weighted outcomes. $\langle E \rangle$ is the mean value as known from basic statistics. Then with the help of spectral decomposition $E = \sum_{i=1} \lambda_i (u_i, \cdot)$ and the identity operator $I = u_i u_i^*$ :

    \begin{align*}
        \langle E \rangle_{\psi} &= \sum_{i} \lambda_i P_{\lambda_i}(\psi) \\
        &= \sum_{i} \lambda_i |(u_i,\psi)|^2 \\
        &= \sum_{i} \lambda_i (\psi,u_i) (u_i,\psi) \\
        &= \sum_{i} (\psi,E u_i) (u_i,\psi)  \\
        &=  (\psi, E (\sum_{i} u_i, u_i)_{L^2} \psi) \\
        \langle E \rangle_{\psi} &=  (\psi, E \psi) .
    \end{align*}

    % one possible source http://physics.mq.edu.au/~jcresser/Phys301/Chapters/Chapter14.pdf

\end{proof}

Note that the expression for the expectation value is very similar to the Rayleigh quotient for a normalized $\psi$. 

Thus far, we have only characterized the problem, but we have no idea what the wave function should look like. One of the most groundbreaking discoveries of quantum mechanics is the Born rule. It states that the square of the wave function at any point is the probability of finding a particle at that location:

\begin{align}
    \| \psi(x) \|^2 = P(x) .
\end{align}

Using the Bohr rule, we can restrict the space of possible wave functions. Imagine that we have a system of one electron. We are expecting that the probability of finding that election anywhere is 100\%. This leads to the so-called normalization condition for the wave function:

\begin{align}
    \| \psi \|_{L^2}^2 =  \int_{\mathbb{R}^{3N}} | \psi(x_1, x_2, \dots, x_N ) |^2 \ dx_1 dx_2 \dots dx_N = 1.
    \label{eq:normalization}
\end{align}

The normalization condition can be applied to any wave function whose norm is less than infinity. Therefore, the possible space for our wave function is the space of square-integrable functions $L^2$.

Instead of an eigenvalue problem, we can restate the search for the ground state as an optimization problem:

\begin{align}
    E_0 = \inf \{ E(\psi), \ \psi \in L^2 (\mathbb{R}^{3N}, \mathbb{C}), \ \| \psi \|_{L^2} = 1 \} .
\end{align}

So far we have seen how to formulate the time-independent Schrödinger equation \eqref{eq:timeIndependentSchrodinger} in terms of the wave functions that reduce the energy functional \eqref{eq:energyFunctional}. However, there is an alternative way. Since the energy of a system, the expectation value of Hamiltonian operator $H$, applied to the space of wave functions $L^2$ does not depend on the specific basis of the operator, but on the spanned space, it is possible to omit individual orbitals from the problem. Then we can focus on the spanned space instead. These spaces are projected by a so-called density operator. The density operator is an elegant way of formulating the ground state problem. Instead of writing the state as a system of an $n$-dimensional state vector, it is written as a projection onto an $n$-dimensional space. The ground state density operator can be stated as

\begin{align}
    \gamma_0 =\sum_{i=1}^{N}  \phi_i \phi_i^* ,
    \label{eq:GroundStateDensityOperator}
\end{align}

where $\phi_i$ are the eigenfunctions corresponding to the $i$-th smallest eigenvalue $\lambda_i$. Before defining the density operator, we need to define the trace and trace class operators.

\begin{definition}[Trace]~\\
    Let $\mathcal{H}$ be a Hilbert space with an orthonormal basis $\{e_i\}_{i=1}^{\infty}$. Then the trace of a positive operator $A$ on $\mathcal{H}$ is given by:
    \begin{align}
        \Tr{A} = \sum_{i=1}^{\infty} (e_i,A e_i) .
    \end{align}     
    The trace of $A$ is independent of the chosen basis $ \{e_i\}_{i=1}^{\infty}$.
\end{definition}

The trace operator is a generalization of the sum of the diagonal elements of a square matrix. Note also that, the trace of an operator $A$ is equivalent to the sum of its eigenvalues. However, because $\mathcal{H}$ is an infinite dimensional space and therefore the basis has infinitely many vectors, not every operator will have a finite trace. Those who do, are part of a special class of operators called trace class $\mathfrak{S}_1$.

\begin{definition}[Trace Class]~\\
    An operator $A$ acting on a Hilbert space $\mathcal{H}$ is called a trace class operator if and only if 
    \begin{align}
        \Tr(|A|) < \infty .
    \end{align}
    The set of trace class operators is denoted by $\mathfrak{S}_1$.
\end{definition}

\begin{definition}[Density Operator]~\\
    The density operator associated with state $\Phi \in L^2$ with the basis $\{\phi_i\}_{i=1}^N$ is defined as:
    \begin{equation}
        \gamma =\sum_{i=1}^{N} \phi_i (\phi_i, \cdot )_{L^2} .
        \label{eq:densityOperator}
    \end{equation}
\end{definition}

\begin{lemma}
    The density operator $\gamma$ has the following properties:
    \begin{enumerate}[label=(\roman*)]
        \item $\gamma \in \mathfrak{S}_1$  
        \item $ \Tr(\gamma) = N$
        \item $\gamma^2 = \gamma$
        \item $\gamma^* = \gamma$
    \end{enumerate} 
\end{lemma}
\begin{proof}
    (i) and (ii): \\
    Let $\{\phi_i\}_{i=1}^\infty$ be any basis of $L^2$ and $\{\phi_i\}_{i=1}^N$ be eigenvectors associated with the density operator $\gamma$, then :
    \begin{align*}
        \Tr(\gamma) = \sum_{i=1}^\infty (\phi_i, \gamma \phi_i) =\sum_{i=1}^\infty \sum_{j=1}^N (\phi_i, \phi_j \phi_j^* \phi_i) = \sum_{i=1}^N (\phi_i, \phi_i) = N < \infty.
    \end{align*}

    (iii):
    \begin{align*}
        \gamma^2 = \sum_i^N \phi_i \phi_i^* \sum_j^N \phi_j \phi_j^* = \sum_i^N \sum_j^N \phi_i \phi_i^*  \phi_j \phi_j^* \\
        = \sum_i^N \sum_j^N \phi_i  \underbrace{\phi_i^* \phi_j}_{\delta_{ij}} \phi_j^* = \sum_i^N \phi_i \phi_i^* = \gamma
    \end{align*}

    (iv):
    \begin{align*}
        (\gamma x, y) = \sum_i^N (\phi \phi^* x)^* y = \sum_i^N (\phi^* x)^* \phi^*  y \\
        = \sum_i^N x^* \phi \phi^*  y = (x, \gamma y) 
    \end{align*}
    Therefore $\gamma$ is self-adjoint and therefore $\gamma^* = \gamma$.
\end{proof}

\begin{theorem}[Expectation Value Density Operator]~\\
    The expectation value of the energy with the state described by the density operator $\gamma$ and Hamiltonian $H$ is given by:

    \begin{align}
        \langle H \rangle_{\gamma} = \Tr(\gamma H) .
    \end{align}

\end{theorem}
\begin{proof} 
    % one version of proof https://homepage.univie.ac.at/reinhold.bertlmann/pdfs/T2_Skript_Ch_9corr.pdf
    In the first step of this proof we insert the definition of the density operator and then write out the trace:
    \begin{align*}
        \Tr(\gamma H) &= \Tr( \sum_{i=1}^{N} \phi_i  \phi_i^* H) \\
        &= \sum_{i=1}^{N} (e_i^* \phi_i)  (\phi_i^* H e_i) \\
        &= \sum_{i=1}^{N} (\phi_i^* H e_i) (e_i^* \phi_i) \\
        &= \sum_{i=1}^{N} (\phi_i^* H \underbrace{e_i ) (e_i^*}_{I} \phi_i) \\
        &= \sum_{i=1}^{N} (\phi_i^* H \phi_i) \\
        &= (\psi, H \psi)
    \end{align*}
\end{proof}

\begin{theorem}[Invariance Under Unitary Transformations]~\\
    The density operator is invariant under unitary transformation. Let U be a unitary operator, $\gamma$ a density operator constructed with the basis $\{\phi_i\}_{i=1}^N$ and $\widetilde{\gamma}$ constructed with $\{\theta_i\}_{i=1}^N$, where $\theta_i = \phi_i U$. Then the following is true:
    \begin{align*}
        \gamma = \widetilde{\gamma} .
    \end{align*}
\end{theorem}
\begin{proof}
    \begin{align*}
        \widetilde{\gamma} &= \sum_{i=1}^N \theta \theta^* \\
        &= \sum_{i=1}^N \phi U (\phi U)^* \\
        &= \sum_{i=1}^N \phi \underbrace{U U^*}_{I} \phi^* \\
        &= \sum_{i=1}^N \phi \phi^* \\
        &= \gamma
    \end{align*}
\end{proof}




Similar to the Schrödinger equation for the state space formulation, you can describe the density operator evolution with a differential equation. The equation that describes the evolution of the density operator is called von Neumann equation.

\begin{theorem}[von Neumann Equation]
    The time evolution of a system given in density operator formalism develops according to the following equation:
    \begin{align*}
        \frac{\partial \gamma}{\partial t} = -i \hbar [H, \gamma] ,
    \end{align*}
    where $[\cdot,\cdot]$ is the matrix commutator.
\end{theorem}
\begin{proof}
    Start with the definition of the density operator \eqref{eq:densityOperator} and take the derivative with respect to time using the product rule:
    \begin{align*}
        \frac{\partial \gamma}{\partial t} &= \sum_{i=1}^N \frac{\partial \phi_i }{\partial t} \phi_i^* + \sum_{i=1}^N \phi_i \frac{\partial \phi_i^*}{\partial t} 
    \end{align*}
    The Schrödinger equations for $\phi$ and $\phi^*$ are:
    \begin{align*}
        i\hbar \frac{\partial \phi}{\partial t} = H \phi \quad \text{, and} \quad -i\hbar \frac{\partial \phi^*}{\partial t} = \phi^*  H
    \end{align*}
    now we insert the partial derivatives into the expression above:
    \begin{align*}
        \frac{\partial \gamma}{\partial t} &= \sum_{i=1}^N (-\frac{i}{\hbar} H \phi_i) \phi_i^* + \sum_{i=1}^N \phi_i (\frac{i}{\hbar} \phi_i^* H) \\
        &= -\frac{i}{\hbar} ( H \sum_{i=1}^N \phi_i \phi_i^* - \sum_{i=1}^N \phi_i \phi_i^* H ) \\
        &= -\frac{i}{\hbar} ( H \gamma - \gamma H) \\
        \frac{\partial \gamma}{\partial t} &= -\frac{i}{\hbar} [H, \gamma]
    \end{align*}
\end{proof}


Having defined and characterized the density operator, we can reformulate the search for the ground state as:
\begin{align}
    E_0 = \inf \; \{ \Tr(H\gamma) \;|\; \gamma \in \mathfrak{S}_1, \gamma = \gamma^2 = \gamma^*, \Tr(\gamma) = N \} .
    \label{eq:EnergyFunctionalDensityOperator}
\end{align}


\section{Example}
Now that we have introduced the density operator, we can show an application with a Hamiltonian that is used in practice. The energy associated with the density operator $\gamma$ can be decomposed into two parts, a linear and a non-linear part:

\begin{align}
    E(\gamma) = E_{\text{lin}}(\gamma) + E_{\text{nlin}}(\gamma) .
\end{align}

The linear part is often called the core Hamiltonian and is built from one particle operators. One particle operators typically describe dynamic variables of one electron, like position and momentum. For the next examples, we will assume that the core Hamiltonian consists of the kinetic energy of the electron and Coulomb interaction with the electric field generated from the nuclei:
\begin{align}
    E_{\text{lin}}(\gamma) = \Tr(H_{\text{core}} \gamma) = \Tr[(-\frac{1}{2} \Delta + V) \gamma] .
\end{align}

This core Hamiltonian is the easy part to compute, the hard part and the difference between models for quantum chemistry is this nonlinear part. It includes the Coulomb interaction between each electron pair and the exchange correlation. The term exchange correlation is one unique to quantum mechanics and therefore probability needs an explanation. Correlation is a term from statistics, and it is a measure on how connected two statistic variables are. In this case, exchange correlation describes the interaction of the electron probability clouds. As we have seen from the Born rule, the location of an electron is a probabilistic matter, therefore its position is usually described not by a position vector, but by a probability cloud. Electrons are part of a particle class called fermions. One of the properties of fermions is that there can not be two electrons in the same state. This creates a repulsion between electrons, the so-called Fermi pressure. This Fermi pressure is one part that has to be taken into account when modeling the exchange correlation term.

We will focus on the Hartree--Fock model. Instead of optimizing in the space of all $L^2$ functions, Hartree--Fock reduces the space of admissible wave functions to the space of anti-symmetrical Slater determinants. We have already said that electrons are fermions. Other properties of fermions are that they are indistinguishable and the exchange of two particles changes the sign of the wave function. Anti-symmetrical Slater determinants fulfill those requirements. The nonlinear contributions to the energy can be written as:

\begin{align}
    E_{\text{nlin}}(\gamma) = \frac{1}{2} \Tr(\mathcal{G}(\gamma) \gamma) ,
    \label{eq:EnergyNonLinear}
\end{align}
where 
\begin{align*}
    (\mathcal{G} \circ \phi)(x) = (\rho_\gamma \star \frac{1}{|y|})(x) \phi (x) - \int_{\mathbb{R}^3} \frac{\tau_\gamma (x, y)}{|x-y|} \phi(y) dy
\end{align*}

and $\tau_\gamma(x,y) = \sum_{i=1}^N \phi(x) \phi(y)^*$ is the kernel of $\gamma$ and $\rho_D(x) = \tau_\gamma(x,x)$. Equation \eqref{eq:EnergyNonLinear}, as well as the following derivation of the Fock equations can be found with more details in \cite{cances2003computational}.

In order to solve the constrained optimization problem \eqref{eq:EnergyFunctionalDensityOperator}, we formulate the Lagrange equation. Then we take the Gâteaux derivative and set it to 0:

\begin{align*}
    &\mathcal{L}(\gamma, \Lambda) = E(\gamma) - \Lambda (\gamma^* \gamma -I) ,\\
    &\frac{\partial}{\partial \epsilon} \mathcal{L}(D_0 + \epsilon \gamma, \Lambda) = 0 .
\end{align*}

This leads to the Fock equations, which are a set of nonlinear eigenvalue equations:

\begin{equation}
    \begin{aligned}
        &F(\gamma) \phi_i = e_i \phi_i \quad i \in [1,\dots, N] ,\\
        &\phi_i^* \phi_j = \delta_{i,j} , \\
        &\gamma = \sum_{i=1}^N \phi_i \phi_j^* ,
    \end{aligned}
\end{equation}

with $F$ being the Fock operator: 

\begin{align}
    (F(\gamma) \circ \phi)(x) = - \frac{1}{2} \Delta \phi(x) + V\phi + (\rho_\gamma \star \frac{1}{|y|})(x)  \phi(x) - \int_{\mathbb{R}^3} \frac{\tau_\gamma(x,y)}{|x-y|} \phi(y) dy .
    \label{eq:FockOperator}
\end{align}

The solution of this nonlinear eigenvalue problem is no easy task. We will first show a way to discretize the problem to a finite-dimensional space using a Galerkin approach and then later present two ways to solve them.
Before solving those equations, we first need to discretize the individual orbitals to arrive at a finite-dimensional problem. For this we will apply a Galerkin approach in which $\phi_i$ gets replaced by $\phi_i^h$ constructed by a finite-dimensional basis $\{\chi_i\}_{i=1}^{N_b}$:

\begin{align}
    \phi(x)_i \approx \phi(x)_i^{h}  = \sum_{j=1}^{N_b} c_{i,j} \chi_{j} ,
\end{align}

where $c_{i,j} \in \mathbb{C}$ are the coefficients we are trying to determine. We combine all the coefficients $c_{i,j}$ to a $\mathbb{C}^{N \times N_b}$ matrix $C$. Therefore, we can write the discrete case of the density operator as:

\begin{equation}
    \gamma = CC^* .
\end{equation}

The choice of the right basis functions for each problem is very important. It influences the accuracy of the result, stability, and computational effort. Because molecules are formed from combinations of atoms, it is a natural idea to use the orbitals of atoms as the basis set. This group of basis functions is called linear combinations of atomic orbitals, or short LCAO. LCAO can be split into two groups. Slater-type orbitals and Gaussian-type orbitals. For periodic structures, for example, crystals, LCAO can be a bad choice due to the large number of atoms. For such structures, plane waves are a popular choice. Plane waves are built upon the Fourier basis functions of the form $e^{-i k x}$ up to a certain energy cutoff. A big advantage of plane waves is the fact that the basis functions form an orthonormal basis.

Since the original $\phi_i$ were eigenfunctions, they had to be orthogonal. This must not be true for the basis functions $\chi_i$. We define the overlap matrix $S \in \mathbb{R}^{N_b \times N_b}$ as:

\begin{align}
    S_{j,k} = (\chi_j,\chi_k)_{L^2} .
\end{align}

The orthonormality constraint for the $\phi_i$ can be stated as:

\begin{align}
    \delta_{i,j} = (\phi_i, \phi_j) = (\sum_{\nu=1}^{N_b} c_{i,\nu} \chi_\nu, \sum_{\mu=1}^{N_b} c_{j,\mu} \chi_\mu) = \sum_{\nu=1}^{N_b} \sum_{\mu=1}^{N_b} c_{i,\nu} S_{\nu,\mu} c_{j,\mu} ,
\end{align}

or equivalent in matrix form as:

\begin{align}
    I = C^* S C .
\end{align}

The discrete Hartree--Fock equations can then be stated as:
\begin{equation}
    \begin{aligned}
    &F(\gamma)C = SCE ,\\
    &C^*SC = I , \\
    &\gamma = CC^*,
    \end{aligned}
    \label{eq:DiscreteHartreeFock}
\end{equation}

where $F(\gamma)$ is called the Fock matrix and is the discretized version of the Fock operator \eqref{eq:FockOperator}.


There are two main approaches to solve \eqref{eq:DiscreteHartreeFock}. The main one is Self-Consistent-Fields (SCF) and the other one is direct minimization on the Grassmann or Stiefel manifold. For a more in-depth analysis of the algorithm including convergence behavior, we refer the reader to \cite{cances2021convergence}.