using Plots
using Compose
using Images
using LaTeXStrings

## create render plot
# load render
render = Array{Any}(undef,6)
plot_render = Array{Plots.Plot{Plots.GRBackend}}(undef,6)
for (index,value) in enumerate([0,2,4,6,8,10])
    render[index] = load("plots/SpiderManRender/t=$value.png")
    img_size = size(render[index])
    render[index] = render[index][ :,floor(Int, 1/5*img_size[2]) : floor(Int, 3/4*img_size[2])]
    plot_render[index] = plot(render[index],xaxis=false,yaxis=false,ylabel="", xlabel="",yticks=[],xticks=[], title="t = $(value/10)",dpi=600,right_margin = -2Plots.mm)
end
plot(plot_render[1],plot_render[2],plot_render[3], plot_render[4], plot_render[5], plot_render[6], layout=(1,:),size=(1200, 300))

