import LinearAlgebra
import Plots
import Manifolds



function circle(h,k,r)
    Θ = LinRange(0,2pi,100)
    h .+ r*sin.(Θ), k.+ r*cos.(Θ)
end

function line(a,b)
    collect(LinRange(a[1],b[1],100)), collect(LinRange(a[2],b[2],100))
end

function points_on_manifold()
    Θ = LinRange(0,2pi,10)
    sin.(Θ), cos.(Θ)
end

function point_on_tangentspace()
    Θ = LinRange(0,2pi,10)
    # U = Tuple
    # for i in Θ
    #     origin + inverse_retract(St, origin, [sin(i),cos(i)],QRInverseRetraction())
    # end
end

# create manifold 
Gr = Grassmann(2,1)
U = points_on_manifold()
origin = [0.0 1.0]'
check_point(Gr,origin)
Θ = 3/4π
point_on_tangentspace()

## start plot construction
plot_total = plot(circle(0,0,1), label="Manifold",size=(5Plots.PX_PER_INCH,5Plots.PX_PER_INCH))
plot!(plot_total,line((-1.25,1),(1.25,1,)), label="Tangent Space",ylim=[-1.25,1.25])
scatter!(points_on_manifold(), c="green", label="Points on Manifold")
