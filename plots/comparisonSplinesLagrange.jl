using LinearAlgebra
using DelimitedFiles
using Dierckx
using Manifolds
using Plots
using Graphs, SimpleWeightedGraphs
using VTKDataIO
using ManifoldsBase
using Interpolations

using ManifoldInterpolation

function lagrange_basis(interpolation_param, t, j)
    n = size(interpolation_param)[1]
    val = 1
    for i in 1:n
        if (i != j)
            val *= (t - interpolation_param[i] )/ ( interpolation_param[j] - interpolation_param[i] )
        end
    end 
    return val
end

function frobenius_norm(U_approx, U_exact)
    norm(U_approx * U_approx' - U_exact*U_exact',2)
end

## load dataset
system_path = "./code/"

# start_range = 0.2
# stop_range  = 3.0

# f(p,arg::Number) = diagm([1,arg,0]) * p  # scale y-direction
# trafo_param_space = [i for i in range(start_range, step=0.01, stop=stop_range)]

# graphs =  CustomGraph[]
# for (index,value) in enumerate(trafo_param_space)
#     graph_tmp = CustomGraph(system_path * "Meshes/square.vtk")
#     graphs = vcat(graphs,graph_tmp)
#     loadMesh(graphs[index])
#     transformMesh(graphs[index],f,value)
#     createLaplacian(graphs[index])
#     computeEigenGraph(graphs[index])
# end 


domain = LinRange(0.2, 3, 256)
f(p,arg::Number) = diagm([1,arg,0]) * p  # scale y-direction
trafo_param_space = [i for i in domain]
trafo_param_space = 0.2:0.01:3

graphs =  CustomGraph[]
for (index,value) in enumerate(trafo_param_space)
    graph_tmp = CustomGraph(system_path * "Meshes/square.vtk")
    graphs = vcat(graphs,graph_tmp)
    loadMesh(graphs[index])
    transformMesh(graphs[index],f,value)
    createLaplacian(graphs[index])
    computeEigenGraph(graphs[index])
end 

##
n_eigenfunctions = 5    # crossing at 6
U = Array{Float64}[] 
map(i -> push!(U,Matrix{Float64}(i.U[:,1:n_eigenfunctions])), graphs)

# n_points = [ 128, 64, 32 ,16 ,8 ,4 ,2, 1]
# interpolation_points = U[1:n_points[end-1]:end]
# interpolation_param = LinRange(start_range, stop_range, size(interpolation_points)[1])

interpolation_param = 0.2:0.1:3
interpolation_points = U[1:10:end]


origin = interpolation_points[1]
Gr = Grassmann(size(U[1])[1], n_eigenfunctions)

interpolated_solution = interpolate_complete(interpolation_points, interpolation_param, trafo_param_space, origin, n_eigenfunctions )
plot_crossing(graphs, n_eigenfunctions, 1,  interpolated_solution, U , trafo_param_space, interpolation_param, Gr)

## Lagrange Interpolation


lagrange_interpolation = []
for i in trafo_param_space
    interpolated_value = zeros(size(itp_sample_points[1])[1], n_eigenfunctions)
    for j in 1:size(interpolation_param)[1]
        interpolated_value += lagrange_basis(interpolation_param, i, j) .* log(Gr, origin, interpolation_points[j])
    end
    push!(lagrange_interpolation, exp(Gr, origin, interpolated_value))
end

error = []
for (i,v) in enumerate(U)
    push!(error, frobenius_norm(v, lagrange_interpolation[i]))
end

plot(error)

plot_itp_error_polynomial = plot_interpolation_error(lagrange_interpolation, U , trafo_param_space, interpolation_param, Gr,"Lagrange Interpolation")

## Interpolation with Splines Linear 
spline_interpolation = []

itp = Matrix{Interpolations.ScaledInterpolation}(undef,size(U[1])[1], n_eigenfunctions)


for i in 1:size(interpolation_points[1])[1]
    for j in 1:n_eigenfunctions
        # itp[i,j] = interpolate([log(Gr,origin,k)[i,j] for k in interpolation_points], BSpline(Cubic(Natural(OnCell())))) |> i->scale(i,interpolation_param)
        itp[i,j] = interpolate([log(Gr,origin,k)[i,j] for k in interpolation_points], BSpline(Linear())) |> i->scale(i,interpolation_param)
    end
end

function interpolate_linear(t)
    val = Matrix{Float64}(undef,size(U[1])[1],n_eigenfunctions)

    for i in 1:size(interpolation_points[1])[1]
        for j in 1:n_eigenfunctions
            val[i,j] = itp[i,j](t)
        end
    end 
    return val
end

for i in trafo_param_space
    interpolated_value = interpolate_linear(i)
    push!(spline_interpolation, exp(Gr, origin, interpolated_value))
end

error_sum = 0.0
for (i,v) in enumerate(spline_interpolation)
    error_sum += frobenius_norm(v, U[i])
end
@info error_sum

plot_itp_error_spline_Linear = plot_interpolation_error(spline_interpolation, U , trafo_param_space, interpolation_param, Gr,"Linear Splines")

## Interpolation with Splines Cubic 
spline_interpolation = []

itp = Matrix{Interpolations.ScaledInterpolation}(undef,size(U[1])[1],n_eigenfunctions)

for i in 1:size(interpolation_points[1])[1]
    for j in 1:n_eigenfunctions
        itp[i,j] = interpolate([log(Gr,origin,k)[i,j] for k in interpolation_points], BSpline(Cubic(Natural(OnCell())))) |> i->scale(i,interpolation_param)
        # itp[i,j] = interpolate([log(Gr,origin,k)[i,j] for k in interpolation_points], BSpline(Linear())) |> i->scale(i,interpolation_param)
    end
end

function interpolate_cubic(t)
    val = Matrix{Float64}(undef,size(U[1])[1],n_eigenfunctions)

    for i in 1:size(interpolation_points[1])[1]
        for j in 1:n_eigenfunctions
            val[i,j] = itp[i,j](t)
        end
    end 
    return val
end

for i in trafo_param_space
    interpolated_value = interpolate_cubic(i)
    push!(spline_interpolation, exp(Gr, origin, interpolated_value))
end

error_sum = 0.0
for (i,v) in enumerate(spline_interpolation)
    error_sum += frobenius_norm(v, U[i])
end
@info error_sum

plot_itp_error_spline_cubic = plot_interpolation_error(spline_interpolation, U , trafo_param_space, interpolation_param, Gr, "Cubic Splines")

## combine Plots

plot( plot_itp_error_polynomial, plot_itp_error_spline_Linear, plot_itp_error_spline_cubic, layout=(:,1), size=(1200,600))


## convergence plots


f(p,arg::Number) = diagm([1,arg,0]) * p  # scale y-direction
trafo_param_space = 0.2:0.001:3

graphs =  CustomGraph[]
for (index,value) in enumerate(trafo_param_space)
    graph_tmp = CustomGraph(system_path * "Meshes/square.vtk")
    graphs = vcat(graphs,graph_tmp)
    loadMesh(graphs[index])
    transformMesh(graphs[index],f,value)
    createLaplacian(graphs[index])
    computeEigenGraph(graphs[index])
end 

n_eigenfunctions = 4    # crossing at 6
U = Array{Float64}[] 
map(i -> push!(U,Matrix{Float64}(i.U[:,1:n_eigenfunctions])), graphs)

##

# distance_itp = [ 0.1, 0.05, 0.01, 0.005]
# errors = Matrix(undef, 3, size(distance_itp)[1])

# for (index,value) in enumerate(distance_itp)
#     @info value

#     interpolation_param = 0.2:value:3
#     interpolation_points = U[1:Integer(value/0.001):end]

#     origin = interpolation_points[1] 
#     Gr = Grassmann(size(U[1])[1], n_eigenfunctions)

#     # linear interpolation
#     itp = Matrix{Interpolations.ScaledInterpolation}(undef,size(U[1])[1],n_eigenfunctions)
#     for i in 1:size(interpolation_points[1])[1]
#         for j in 1:n_eigenfunctions
#             itp[i,j] = interpolate([log(Gr,origin,k)[i,j] for k in interpolation_points], BSpline(Linear())) |> i->scale(i,interpolation_param)
#         end
#     end

#     function interpolate_linear(t)
#         val = Matrix{Float64}(undef,size(U[1])[1],n_eigenfunctions)

#         for i in 1:size(interpolation_points[1])[1]
#             for j in 1:n_eigenfunctions
#                 val[i,j] = itp[i,j](t)
#             end
#         end 
#         return val
#     end
#     interpolate_linear_2 = interpolate(interpolation_points, BSpline(Linear())) |> i -> scale(i,interpolation_param)
    
#     error_sum = 0.0
#     for (index2,value2) in enumerate(trafo_param_space)
#         error_sum += frobenius_norm(exp(Gr,origin,interpolate_linear(value2)), U[index2])
#     end
#     println(error_sum)
#     errors[1,index] = error_sum

#     # cubic Splines

#     itp = Matrix{Interpolations.ScaledInterpolation}(undef,size(U[1])[1],n_eigenfunctions)

#     for i in 1:size(interpolation_points[1])[1]
#         for j in 1:n_eigenfunctions
#             itp[i,j] = interpolate([log(Gr,origin,k)[i,j] for k in interpolation_points], BSpline(Cubic(Natural(OnCell())))) |> i->scale(i,interpolation_param)
#         end
#     end

#     function interpolate_cubic_2(t)
#         val = Matrix{Float64}(undef,size(U[1])[1],n_eigenfunctions)

#         for i in 1:size(interpolation_points[1])[1]
#             for j in 1:n_eigenfunctions
#                 val[i,j] = itp[i,j](t)
#             end
#         end 
#         return val
#     end

#     error_sum = 0.0
#     for (index2,value2) in enumerate(trafo_param_space)
#         error_sum += frobenius_norm(exp(Gr,origin,interpolate_cubic_2(value2)), U[index2])
#     end
#     println(error_sum)
#     errors[2,index] = error_sum


#     # #lagrange interpolation
#     # function interpolate_lagrange(t)
#     #     interpolated_value = zeros(size(itp_sample_points[1])[1], n_eigenfunctions)
#     #     for j in 1:size(interpolation_param)[1]
#     #         interpolated_value += lagrange_basis(interpolation_param, t, j) .* log(Gr, origin, interpolation_points[j])
#     #     end
#     #     return interpolated_value
#     # end

#     # error_sum = 0.0
#     # for (index,value) in enumerate(trafo_param_space)
#     #     error_sum += frobenius_norm(exp(Gr,origin,interpolate_lagrange(value)), U[index])
#     # end
#     # @info error_sum
#     # error[3,index] = error_sum

# end

## lagrange interpolation at the Chebyshev Polynomials
# computing the Chebyshev nodes:
nodes_chebyshev(a, b, n) = begin
    nodes_array = []
    for k in 1:n
       push!(nodes_array, 0.5*(a+b) + 0.5*(a-b) * cos((2*k-1)/(2*n) *pi) )
    end
    return nodes_array
end


n_points = [5, 10 , 20 ,40, 60, 80, 100]
error_frobenius = Matrix(undef, 4, size(n_points)[1])
error_angle = Matrix(undef, 4, size(n_points)[1])

for (index,value) in enumerate(n_points)
    @info value

    
    nodes_itp = nodes_chebyshev(0.2,3,value)
    interpolation_param = [ trafo_param_space[findmin(abs.(trafo_param_space .- i))[2]] for i in nodes_itp]
    interpolation_points = [U[i] for i in [ findmin(abs.(trafo_param_space .- i))[2] for i in nodes_itp]]
    
    origin = interpolation_points[1] 
    Gr = Grassmann(size(U[1])[1], n_eigenfunctions)
    #lagrange interpolation
    function interpolate_lagrange(t)
        interpolated_value = zeros(size(interpolation_points[1])[1], n_eigenfunctions)
        for j in 1:size(interpolation_param)[1]
            interpolated_value += lagrange_basis(interpolation_param, t, j) .* log(Gr, origin, interpolation_points[j])
        end
        return interpolated_value
    end

    error_sum = 0.0
    for (index,value) in enumerate(trafo_param_space)
        error_sum += frobenius_norm(exp(Gr,origin,interpolate_lagrange(value)), U[index])
    end
    @info error_sum
    error_frobenius[1,index] = error_sum

    error_sum = 0.0
    for (index,value) in enumerate(trafo_param_space)
        error_sum += distance(Gr, exp(Gr,origin,interpolate_lagrange(value)), U[index])
    end
    error_angle[1,index] = error_sum


    #linear Splines
    nodes_itp = LinRange(0.2,3,value)
    interpolation_param = [ trafo_param_space[findmin(abs.(trafo_param_space .- i))[2]] for i in nodes_itp]
    interpolation_points = [U[i] for i in [ findmin(abs.(trafo_param_space .- i))[2] for i in nodes_itp]]


    itp = Matrix{Any}(undef,size(U[1])[1],n_eigenfunctions)
    for i in 1:size(U[1])[1]
        for j in 1:n_eigenfunctions
            # itp[i,j] = interpolate((interpolation_param,), [log(Gr,origin,k)[i,j] for k in interpolation_points], Gridded(Linear()))
            itp[i,j] = Spline1D(interpolation_param, [log(Gr,origin,k)[i,j] for k in interpolation_points], k=1)
        end
    end

    function interpolate_linear(t)
        val = Matrix{Float64}(undef,size(U[1])[1],n_eigenfunctions)

        for i in 1:size(interpolation_points[1])[1]
            for j in 1:n_eigenfunctions
                val[i,j] = itp[i,j](t)
            end
        end 
        return val
    end
    
    error_sum = 0.0
    for (index2,value2) in enumerate(trafo_param_space)
        error_sum += frobenius_norm(exp(Gr,origin,interpolate_linear( value2)) , U[index2])
    end
    @info error_sum
    error_frobenius[2,index] = error_sum

    error_sum = 0.0
    for (index,value) in enumerate(trafo_param_space)
        error_sum += distance(Gr, exp(Gr,origin,interpolate_linear(value)), U[index])
    end
    error_angle[2,index] = error_sum

    #cubic splines
    nodes_itp = LinRange(0.2,3,value)
    interpolation_param = [ trafo_param_space[findmin(abs.(trafo_param_space .- i))[2]] for i in nodes_itp]
    interpolation_points = [U[i] for i in [ findmin(abs.(trafo_param_space .- i))[2] for i in nodes_itp]]


    itp = Matrix{Any}(undef,size(U[1])[1],n_eigenfunctions)
    for i in 1:size(U[1])[1]
        for j in 1:n_eigenfunctions
            # itp[i,j] = interpolate((interpolation_param,), [log(Gr,origin,k)[i,j] for k in interpolation_points], Gridded(Linear()))
            itp[i,j] = Spline1D(interpolation_param, [log(Gr,origin,k)[i,j] for k in interpolation_points], k=3)
        end
    end

    function interpolate_cubic(t)
        val = Matrix{Float64}(undef,size(U[1])[1],n_eigenfunctions)

        for i in 1:size(interpolation_points[1])[1]
            for j in 1:n_eigenfunctions
                val[i,j] = itp[i,j](t)
            end
        end 
        return val
    end
    
    error_sum = 0.0
    for (index2,value2) in enumerate(trafo_param_space)
        error_sum += frobenius_norm(exp(Gr,origin,interpolate_cubic(value2)) , U[index2])
    end
    @info error_sum
    error_frobenius[3,index] = error_sum

    error_sum = 0.0
    for (index,value) in enumerate(trafo_param_space)
        error_sum += distance(Gr, exp(Gr,origin,interpolate_cubic(value)), U[index])
    end
    error_angle[3,index] = error_sum


    #lagrange interpolation
    nodes_itp = LinRange(0.2,3,value)
    interpolation_param = [ trafo_param_space[findmin(abs.(trafo_param_space .- i))[2]] for i in nodes_itp]
    interpolation_points = [U[i] for i in [ findmin(abs.(trafo_param_space .- i))[2] for i in nodes_itp]]


    function interpolate_lagrange(t)
        interpolated_value = zeros(size(interpolation_points[1])[1], n_eigenfunctions)
        for j in 1:size(interpolation_param)[1]
            interpolated_value += lagrange_basis(interpolation_param, t, j) .* log(Gr, origin, interpolation_points[j])
        end
        return interpolated_value
    end

    error_sum = 0.0
    for (index,value) in enumerate(trafo_param_space)
        error_sum += frobenius_norm(exp(Gr,origin,interpolate_lagrange(value)), U[index])
    end
    @info error_sum
    error_frobenius[4,index] = error_sum

    error_sum = 0.0
    for (index,value) in enumerate(trafo_param_space)
        error_sum += distance(Gr, exp(Gr,origin,interpolate_lagrange(value)), U[index])
    end
    error_angle[4,index] = error_sum
end

##
using LaTeXStrings

default(; # Plots defaults
    fontfamily="Computer modern",
    label="" ,# only explicit legend entries
    dpi= 1000,
    legendfontsize = 11,
    labelfontsize=12,
    titlefontsize=14,
)

# frobenius norm error
plot( [n_points] ,error_frobenius[1,:], yaxis=:log, xaxis=:log, markershape=:auto ,label="Lagrange at Chebyshev Nodes")
plot!([n_points] ,error_frobenius[4,:], yaxis=:log, xaxis=:log, markershape=:auto ,label="Lagrange evenly spaced Grid")
plot!([n_points] ,error_frobenius[2,:], yaxis=:log, xaxis=:log, markershape=:auto ,label="Linear Splines")
plot!([n_points] ,error_frobenius[3,:], yaxis=:log, xaxis=:log, markershape=:auto ,label="Cubic Splines",
                    legend=:bottomleft,
                    title = "Convergence Rates of Different Interpolation Methods",
                    xticks=(n_points, n_points),
                    xlabel="Interpolation Nodes",
                    ylabel=L"\textrm{Error :} \quad  \|\|  U_{int}U_{int}^T - U_{ref}U_{ref}^T  \|\|\ _{F} ",
                    margin=5Plots.mm,
                    size=(
                        1000,600))
# savefig("./latex/bachelorThesis/figs/convergence_rates.png")

# angle norm
plot( [n_points] ,error_angle[1,:], yaxis=:log, xaxis=:log, markershape=:auto ,label="Lagrange at Chebyshev Nodes")
plot!([n_points] ,error_angle[4,:], yaxis=:log, xaxis=:log, markershape=:auto ,label="Lagrange evenly spaced Grid")
plot!([n_points] ,error_angle[2,:], yaxis=:log, xaxis=:log, markershape=:auto ,label="Linear Splines")
plot!([n_points] ,error_angle[3,:], yaxis=:log, xaxis=:log, markershape=:auto ,label="Cubic Splines",
                    legend=:bottomleft,
                    title = "Convergence Rates of Different Interpolation Methods",
                    xticks=(n_points, n_points),
                    xlabel="Interpolation Nodes",
                    ylabel=L"\textrm{Error :} \quad  \|\|  U_{int}U_{int}^T - U_{ref}U_{ref}^T  \|\|\ _{F} ",
                    margin=5Plots.mm,
                    size=(
                        1000,600))


## export errors as CSV
export_frobenius = Matrix(undef,5,7)
export_frobenius[1,:] = n_points
export_frobenius[2:end,:] = error_frobenius

writedlm( "./latex/bachelorThesis/figs/export_frobenius.csv",  export_frobenius', ',')


export_angle= Matrix(undef,5,7)
export_angle[1,:] = n_points
export_angle[2:end,:] = error_angle

writedlm( "./latex/bachelorThesis/figs/export_angle.csv",  export_angle', ',')