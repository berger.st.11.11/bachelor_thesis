using Manifolds, Plots, LinearAlgebra
using DelimitedFiles

# f(x) = -0.6*sin(2*π *x)
f(x) = (+-0.175*(2+ 6/2*x)^3+2.5083*(2+ 6/2*x)^2+-10.216*(2+ 6/2*x)^1+14.8*(2+ 6/3*x)^0 - 3)/3
St = MetricManifold(Stiefel(3,1),CanonicalMetric())
samples = 50


domain = LinRange(0,2 ,samples)

t_space_elements = [ [f(x); x; 0] for x in domain ]

t_space_elements = reduce(vcat, collect(t_space_elements)')

manifold_elements = []
origin = [0.0; 0.0; 1.0]
origin = reshape(origin,3,1)

for i in 1:size(domain)[1]
    push!(manifold_elements, exp(St, origin, t_space_elements[i,:]))
end

manifold_elements = reduce(vcat, collect(manifold_elements)')

# writedlm(" /retractionManifold.csv", manifold_elements, ",")
# writedlm("./latex/bachelorThesis/figs/ManifoldInterpolation/retractionManifold.csv", manifold_elements, ",")

# plot(manifold_elements[:,2])

St = MetricManifold(Stiefel(2,1),CanonicalMetric())
St = Stiefel(2,1)
retract(St, reshape([0.;1.],2,1), [pi/2; 0])